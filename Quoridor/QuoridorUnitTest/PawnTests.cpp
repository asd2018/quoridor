#include "CppUnitTest.h"
#include "stdafx.h"
#include "Pawn.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(PawnTests)
	{

	public:

		TEST_METHOD(Constructor)
		{
			Pawn pawn(Pawn::Body::SecondPlayer);
			Assert::IsTrue(Pawn::Body::SecondPlayer == pawn.getBody());
		}
		
		TEST_METHOD(EmptyConstructor)
		{
			Pawn pawn;
			Assert::IsFalse(pawn.getBody() != Pawn::Body::None);
		}

		TEST_METHOD(MoveConstructor)
		{
			Pawn pawnFirstPlayer(Pawn::Body::SecondPlayer);
			Pawn otherPawn(std::move(pawnFirstPlayer));
			Assert::IsTrue(otherPawn.getBody() == Pawn::Body::SecondPlayer);
		}

		TEST_METHOD(CopyAssigment)
		{
			Pawn firstPawn(Pawn::Body::SecondPlayer);
			Pawn secondPawn(Pawn::Body::SecondPlayer);
			firstPawn = secondPawn;
			Assert::IsFalse(firstPawn.getBody() != Pawn::Body::SecondPlayer);
		}

		TEST_METHOD(CopyConstructor)
		{
			Pawn firstPawn(Pawn::Body::SecondPlayer);
			Pawn secondPawn(firstPawn);
			Assert::IsTrue(firstPawn.getBody() == secondPawn.getBody());
		}

		TEST_METHOD(SettersAndGetters)
		{
			Pawn pawn;
			pawn.setBody(Pawn::Body::SecondPlayer);
			pawn.setPosition(3);
			if (pawn.getBody() != Pawn::Body::SecondPlayer || pawn.getPosition() != 3)
				Assert::Fail();
		}

	};
}