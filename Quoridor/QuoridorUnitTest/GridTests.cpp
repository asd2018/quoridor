#include "stdafx.h"
#include "CppUnitTest.h"
#include "Grid.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(GridTests)
	{
	public:

		TEST_METHOD(ConstructorWithParameters)
		{
			int numberRows = 9;
			Grid grid(numberRows);
			bool proper;
			std::vector<std::vector<Vertex>> list = grid.getAdjacencyLists();
			int size = list.size();
			proper = (size == numberRows * numberRows);

			if (proper)
			{
				for (int index = 0; index < size; ++index)
				{
					if (list[index][0].getId() != index)
					{
						proper = false;
						break;
					}
					if (index < numberRows)
					{
						if (list[index][0].getType() != Vertex::Type::Destination2)
						{
							proper = false;
							break;
						}
					}
					if (index > size - numberRows - 1)
					{
						if (list[index][0].getType() != Vertex::Type::Destination1)
						{
							proper = false;
							break;
						}
					}
				}
			}
			Assert::IsTrue(proper);
		}

		TEST_METHOD(SquareBracketsOperator)
		{
			Grid grid(3);
			Vertex vertex(8, Vertex::Type::Destination1, Pawn::Body::None);
			Assert::IsTrue(grid[8] == vertex);
		}

		TEST_METHOD(HasEdge)
		{
			Grid grid(3);
			Assert::IsTrue((grid.hasEdge(1, 2)) && !(grid.hasEdge(1, 8)));
		}

		TEST_METHOD(AddEdge)
		{
			Grid grid(3);
			grid.addEdge(1, 8);
			Assert::IsTrue(grid.hasEdge(1, 8));
		}

		TEST_METHOD(RemoveEdge)
		{
			Grid grid(3);
			grid.removeEdge(1, 2);
			Assert::IsFalse(grid.hasEdge(1, 2));
		}

		TEST_METHOD(PredecesorVector)
		{
			Grid grid(5);
			grid.removeEdge(23, 24);
			grid.removeEdge(19, 24);
			std::vector<int> predVector = grid.generatePredecessorVector(0);
			Assert::IsTrue(predVector[24] == -2);
		}
	};
}