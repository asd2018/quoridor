#include "stdafx.h"
#include "CppUnitTest.h"
#include "Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(BoardTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			Board board(2);
			size_t sizeBoard = 2 * 2;
			Assert::AreEqual(board.getGrid().getAdjacencyLists().size(), sizeBoard);
		}

		TEST_METHOD(CheckWinner)
		{
			Board board(9);

			Pawn pawnPlayer(Pawn::Body::SecondPlayer);
			pawnPlayer.setPosition(7);

			board.getGrid()[pawnPlayer.getPosition()].setPawn(pawnPlayer.getBody());

			Assert::IsTrue(board.getGrid()[pawnPlayer.getPosition()].getType() == Vertex::Type::Destination2);
		}

		TEST_METHOD(checkWinCondition)
		{
			Board board(4);

			Pawn pawnPlayer(Pawn::Body::SecondPlayer);
			pawnPlayer.setPosition(7);

			std::vector<int> condition = board.getGrid().generatePredecessorVector(pawnPlayer.getPosition());
			bool answer = false;

			for (int index = 0; index < 4; ++index)
			{
				if (condition[index] != -2)
					answer = true;
			}
			Assert::IsTrue(answer);
		}

		TEST_METHOD(Getter)
		{
			Board board(2);
			Assert::IsTrue(board.getGrid().getAdjacencyLists().size() == 4);
		}
	};
}