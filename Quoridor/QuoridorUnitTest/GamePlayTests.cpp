#include "stdafx.h"
#include "CppUnitTest.h"
#include "GamePlay.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(GamePlayTests)
	{
	public:

		TEST_METHOD(AllWallsPlacedOnFirstLine)
		{
			Board board(9);
			Pawn firstPawn(Pawn::Body::FirstPlayer);
			firstPawn.setPosition(76);
			Player playerOne("NoOne", firstPawn);
			board.getGrid()[firstPawn.getPosition()].setPawn(Pawn::Body::FirstPlayer);


			Pawn secondPawn(Pawn::Body::SecondPlayer);
			secondPawn.setPosition(4);
			Player playerTwo("Someone", secondPawn);
			board.getGrid()[secondPawn.getPosition()].setPawn(Pawn::Body::SecondPlayer);


			std::string a = "a1";
			std::string b = "a2";
			std::string c = "right";
			playerOne.placeWall(a, b, c, board, playerTwo);

			std::string d = "c1";
			std::string e = "c2";
			std::string f = "right";
			playerOne.placeWall(d, e, f, board, playerTwo);


			std::string g = "e1";
			std::string h = "e2";
			std::string i = "right";
			playerOne.placeWall(g, h, i, board, playerTwo);


			std::string j = "g1";
			std::string k = "g2";
			std::string l = "right";
			playerOne.placeWall(j, k, l, board, playerTwo);

			Assert::IsTrue(board.checkWinCondition(secondPawn.getPosition(), playerTwo.getPlayerNumber()));
		}

		TEST_METHOD(AllWallsPlacedOnFirstColumn)
		{
			Board board(9);
			Pawn firstPawn(Pawn::Body::FirstPlayer);
			firstPawn.setPosition(76);
			Player playerOne("Hello", firstPawn);
			board.getGrid()[firstPawn.getPosition()].setPawn(Pawn::Body::FirstPlayer);


			Pawn secondPawn(Pawn::Body::SecondPlayer);
			secondPawn.setPosition(4);
			Player playerTwo("Hey", secondPawn);
			board.getGrid()[secondPawn.getPosition()].setPawn(Pawn::Body::SecondPlayer);


			std::string a = "a1";
			std::string b = "b1";
			std::string c = "down";
			playerOne.placeWall(a, b, c, board, playerTwo);

			std::string d = "a3";
			std::string e = "b3";
			std::string f = "down";
			playerOne.placeWall(d, e, f, board, playerTwo);


			std::string g = "a5";
			std::string h = "b5";
			std::string i = "down";
			playerOne.placeWall(g, h, i, board, playerTwo);


			std::string j = "a7";
			std::string k = "b7";
			std::string l = "down";
			playerOne.placeWall(j, k, l, board, playerTwo);

			Assert::IsTrue(board.checkWinCondition(secondPawn.getPosition(), playerTwo.getPlayerNumber()));
		}

		TEST_METHOD(NoPawnPlaced)
		{
			Board board(9);
			Pawn pawnOne(Pawn::Body::FirstPlayer);
			pawnOne.setPosition(76);

			Pawn pawnTwo(Pawn::Body::SecondPlayer);
			pawnOne.setPosition(4);

			if (board.getGrid()[pawnOne.getPosition()].getPawn().getBody() == pawnOne.getBody() &&
				board.getGrid()[pawnTwo.getPosition()].getPawn().getBody() == pawnTwo.getBody())
				Assert::Fail();
		}

		TEST_METHOD(PawnPlaced)
		{
			Board board(9);
			Pawn pawnOne(Pawn::Body::FirstPlayer);
			pawnOne.setPosition(76);
			board.getGrid()[pawnOne.getPosition()].setPawn(pawnOne.getBody());

			if (board.getGrid()[pawnOne.getPosition()].getPawn().getBody() != pawnOne.getBody())
				Assert::Fail();
		}

		TEST_METHOD(WinPlayerOne)
		{
			Board board(9);
			Pawn firstPawn(Pawn::Body::FirstPlayer);
			firstPawn.setPosition(15);
			Player playerOne("justME", firstPawn);
			board.getGrid()[firstPawn.getPosition()].setPawn(Pawn::Body::FirstPlayer);


			Pawn secondPawn(Pawn::Body::SecondPlayer);
			secondPawn.setPosition(51);
			Player playerTwo("Us", secondPawn);
			board.getGrid()[secondPawn.getPosition()].setPawn(Pawn::Body::SecondPlayer);

			if (board.getGrid()[secondPawn.getPosition() + 9].getType() == Vertex::Type::Destination1)
				Assert::Fail();
			else
			{
				if (board.getGrid()[firstPawn.getPosition() - 9].getType() != Vertex::Type::Destination2)
					Assert::Fail();
			}
		}

	};
}