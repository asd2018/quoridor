#include "stdafx.h"
#include "CppUnitTest.h"
#include "Vertex.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(VertexTests)
	{
	public:

		TEST_METHOD(EmptyConstructor)
		{
			Vertex vertex;
			Assert::IsTrue((vertex.getId() == 0) && (vertex.getPawn().getBody() == Pawn::Body::None) && (vertex.getType() == Vertex::Type::None));
		}

		TEST_METHOD(ConstructorWithParameters)
		{
			Vertex vertex(13, Vertex::Type::Destination2, Pawn::Body::FirstPlayer);
			Assert::IsTrue((vertex.getId() == 13) && (vertex.getPawn().getBody() == Pawn::Body::FirstPlayer) && (vertex.getType() == Vertex::Type::Destination2));
		}

		TEST_METHOD(CopyConstructor)
		{
			Vertex initialVertex(13, Vertex::Type::Destination2, Pawn::Body::FirstPlayer);
			Vertex vertex(initialVertex);
			Assert::IsTrue((vertex.getId() == 13) && (vertex.getPawn().getBody() == Pawn::Body::FirstPlayer) && (vertex.getType() == Vertex::Type::Destination2));
		}

		TEST_METHOD(EqualVertexesComparison)
		{
			Vertex vertex1(13, Vertex::Type::Destination2, Pawn::Body::FirstPlayer);
			Vertex vertex2(13, Vertex::Type::Destination2, Pawn::Body::FirstPlayer);
			Assert::IsTrue(vertex1 == vertex2);
		}

		TEST_METHOD(DifferentVertexesComparison)
		{
			Vertex vertex1(13, Vertex::Type::Destination2, Pawn::Body::FirstPlayer);
			Vertex vertex2(14, Vertex::Type::Destination1, Pawn::Body::None);
			Assert::IsFalse(vertex1 == vertex2);
		}

		TEST_METHOD(Setters)
		{
			Vertex vertex;
			vertex.setId(13);
			vertex.setPawn(Pawn::Body::FirstPlayer);
			vertex.setType(Vertex::Type::Destination2);
			Assert::IsTrue((vertex.getId() == 13) && (vertex.getPawn().getBody() == Pawn::Body::FirstPlayer) && (vertex.getType() == Vertex::Type::Destination2));
		}

	};
}