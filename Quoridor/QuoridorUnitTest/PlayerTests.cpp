#include "stdafx.h"
#include "CppUnitTest.h"
#include "Player.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuoridorUnitTest
{
	TEST_CLASS(PlayerTests)
	{
	public:


		TEST_METHOD(PlayerConstructor)
		{
			Pawn pawn;
			Player player("testName", pawn);
			Assert::IsTrue(pawn.getBody() == Pawn::Body::None && player.getName() == "testName");

		}

		TEST_METHOD(WallDecrement)
		{
			Pawn pawn = Pawn::Body::FirstPlayer;
			Pawn pawn2 = Pawn::Body::SecondPlayer;
			Board board(9);

			Player player("testName", pawn);
			Player player2("testName2", pawn2);

			std::string a = "a1";
			std::string b = "a2";
			std::string c = "right";

			player.placeWall(a,b,c, board, player2);

			Assert::IsTrue(player.getWallNumber() == 9);
		}


		TEST_METHOD(WallsCheck)
		{
			Pawn pawn = Pawn::Body::FirstPlayer;
			Player player("testName", pawn);
			if (player.getWallNumber() != 10)
				Assert::IsFalse;
		}


		TEST_METHOD(placeWallDeletingEdges)
		{
			Pawn pawn = Pawn::Body::FirstPlayer;
			Pawn pawn2 = Pawn::Body::SecondPlayer;
			Board board(9);

			Player player("1", pawn);
			Player player2("2", pawn2);

			std::string a = "a1";
			std::string b = "a2";
			std::string c = "right";

			player.placeWall(a, b, c, board, player2);

			//checks to see if the edges are ideed deleted
			Assert::IsFalse(board.getGrid().hasEdge(0, 9));
			Assert::IsFalse(board.getGrid().hasEdge(1, 10));
		}
	};
}