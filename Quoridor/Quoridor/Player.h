#pragma once
#include <regex>
#include <string>
#include "Board.h"
#include "../ConsolePrint/Console.h"

class Player
{
public:
	Player(const std::string& name, Pawn pawn);
	bool movePawn(std::istream& in, Grid &grid, int enemyPosition = 0); 
	void placeWall(std::string position, std::string position2, std::string direction, Board & board, Player & enemy);
	int getWallNumber() const;
	int getPawnPosition();
	int getPlayerNumber() const;
	std::string& getName();

	friend std::ostream& operator << (std::ostream& os, const Player& player);

private:
	std::string m_name;
	int m_pawnPosition;
	Pawn m_pawn;
	int m_wallNumber;
};
