#pragma once
#include"Pawn.h"

class Vertex
{
public:

	// Describes the location type, Destination1 is Player1's goal and Destination2 is Player2's goal
	enum class Type : uint8_t
	{
		None,
		Destination1,
		Destination2
	};

	Vertex();

	// Constructor takes an id, the type of vertex and which pawn occupies the space
	Vertex(uint8_t id, Type type = Type::None, Pawn::Body body = Pawn::Body::None);
	Vertex(const Vertex & other);
	~Vertex();

	Vertex& operator= (const Vertex& other);

	// Used whenever we need to find a vertex in a vector of vertexes
	// However it only checks if the ids match
	// As such the ids should be unique
	bool operator == (const Vertex other) const;

	// getters
	uint8_t getId();
	Type getType();
	Pawn& getPawn();

	// setters
	void setId(uint8_t id);
	void setType(Type type);
	void setPawn(Pawn::Body body);

private:
	uint8_t m_id;
	Pawn m_pawn;
	Type m_type : 2;
};

