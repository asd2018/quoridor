#include "Vertex.h"

Vertex::Vertex() :
	Vertex(0)
{
	// empty
}

Vertex::Vertex(uint8_t id, Type type, Pawn::Body body):
	m_id(id),
	m_type(type),
	m_pawn(Pawn(body))
{
	static_assert(sizeof(*this) <= 4, "This class should be 4 bytes in size");
}

Vertex::Vertex(const Vertex & other)
{
	*this = other;
}

Vertex::~Vertex()
{
	m_id = 0;
	m_type = Type::None;
}

Vertex & Vertex::operator=(const Vertex & other)
{
	m_id = other.m_id;
	m_pawn = other.m_pawn;
	m_type = other.m_type;

	return *this;
}

bool Vertex::operator==(const Vertex other) const
{
	return m_id == other.m_id;
}

uint8_t Vertex::getId()
{
	return m_id;
}

Vertex::Type Vertex::getType()
{
	return m_type;
}

Pawn& Vertex::getPawn()
{
	return m_pawn;
}

void Vertex::setId(uint8_t id)
{
	m_id = id;
}

void Vertex::setType(Type type)
{
	m_type = type;
}

void Vertex::setPawn(Pawn::Body body)
{
	m_pawn.setBody(body);
}
