#include "GamePlay.h"

const int TOTAL_ROWS = 9;
const int FIRST_PAWN_POSITION = TOTAL_ROWS / 2; //4
const int SECOND_PAWN_POSITION = TOTAL_ROWS * TOTAL_ROWS - (TOTAL_ROWS + 1) / 2; //76

void GamePlay::gameOn()
{
	std::ofstream logFile("syslog.log", std::ios::app);
	Logger logger(logFile);
	logger.log("Game started", Logger::Level::Info);

	char pause, decisionChar;
	int decision, valueExit = 0;
	char temp;
	
	Console::printTitleScreen();
	
	// This loop ensures that after exiting from the help panel, the main menu will be shown again
	// Exit condition: Quitting in the main menu or winning the game
	while (true)
	{
		Console::printMainMenu();
		while (true)
		{
			try
			{
				std::cin >> temp;
				if ((temp != '0') && (temp != '1') && (temp != '2'))
					throw (std::string) "This choice does not exist! Try again!";
			}
			catch (std::string error)
			{
				Console::printMessage(error);
				logger.log("Player introduced an incorrect input", Logger::Level::Warning);
				continue;
			}
			break;
		}
		switch (temp)
		{
		case '0': 
		{
			Console::printMessage("Exiting...");
			logger.log("Game Over", Logger::Level::Info);
					logFile.close();
			return;
		}
		case '1':
		{
			Board board(TOTAL_ROWS);
			Pawn firstPawn, secondPawn;
			std::string position, position2, direction;
			int playerNumber = 1, playerTwo = 2;

			firstPawn.setPosition(FIRST_PAWN_POSITION);
			firstPawn.setBody(Pawn::Body::FirstPlayer);
			board.getGrid()[FIRST_PAWN_POSITION].setPawn(Pawn::Body::FirstPlayer);

			secondPawn.setPosition(SECOND_PAWN_POSITION);
			secondPawn.setBody(Pawn::Body::SecondPlayer);
			board.getGrid()[SECOND_PAWN_POSITION].setPawn(Pawn::Body::SecondPlayer);

			std::string playerName;
			system("cls");

			Console::printMessage("Player 1 name: ");
			std::cin >> playerName;
			Player firstPlayer(playerName, firstPawn);

			system("cls");
			Console::printMessage("Player 2 name: ");
			std::cin >> playerName;
			Player secondPlayer(playerName, secondPawn);

			// Game loop
			while (true)
			{
				system("cls");
				Console::printMatrix(board.getGrid().toMatrix());
				Console::printUI(firstPlayer.getWallNumber(), firstPlayer.getName(), firstPlayer.getPlayerNumber());

				// Checks if the input is good
				// This will loop until the player inserts the right input
				while (true)
				{
					try
					{
						std::cin >> temp;
						if ((temp != '1') && (temp != '2'))
							throw (std::string) "This choice does not exist! Try again!";
						if ((temp == '2') && (firstPlayer.getWallNumber() == 0))
							throw (std::string) "This choice does not exist! Try again!";
					}
					catch (std::string error)
					{
						Console::printMessage(error);
						logger.log(firstPlayer.getName() + " introduced an incorrect input", Logger::Level::Warning);
						continue;
					}
					break;
				}

				decision = temp - 48;

				switch (decision - 1)
				{
				case 0:
				{
					firstPlayer.movePawn(std::cin, board.getGrid());
					break;
				}
				case 1:
				{
					while (true)
					{
						Console::printMessage("Specify the starting positions(e.g. a1 b1) and direction for the wall\n The directions are: up, down, left, right");
						std::cin >> position >> position2 >> direction;
						try
						{
							firstPlayer.placeWall(position, position2, direction, board, secondPlayer);
						}
						catch (std::string error)
						{
							Console::printMessage(error + "Try again!");
							logger.log(firstPlayer.getName() + " introduced an incorrect input", Logger::Level::Warning);
							continue;
						}
						break;
					}
					break;
				}
				}

				// Victory condition check
				if (board.checkWinner(board.getGrid()[firstPlayer.getPawnPosition()]))
				{
					system("cls");
					Console::printMatrix(board.getGrid().toMatrix());
					Console::printWin(firstPlayer.getName());
					_getch();
					Console::printMessage("Exiting...");
					logger.log(firstPlayer.getName() + " won the game", Logger::Level::Info);
					logger.log("Game Over", Logger::Level::Info);
					logFile.close();
					return;
				}
				// Changes the active player
				std::swap(firstPlayer, secondPlayer);
				std::swap(playerNumber, playerTwo);
			}
			break;
		}
		case '2':
		{
			Console::printHelp();
			while (true)
			{
				std::cin >> decisionChar;
				if (decisionChar == 'M')
				{
					break;
				}
			}
			break;
		}
		}
	}
	logFile.close();
}

