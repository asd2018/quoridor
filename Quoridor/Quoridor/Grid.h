#pragma once
#include "Vertex.h"
#include <vector>
#include <algorithm>

// This is a graph where each node can have a maximum of 4 connections
// It is a graph that looks like a squared matrix
class Grid
{
public:
	Grid();
	// Creates a numberRows * numberRows size grid
	Grid(int numberRows);
	~Grid();

	// With this operator we can access directly an element of the grid
	Vertex & operator[] (int number);

	// Removes the edge between the first node and the second
	void removeEdge(int node1, int node2);

	// Adds an edge between the first node and the second
	void addEdge(int node1, int node2);

	// Checks if the two nodes have an edge connecting them
	bool hasEdge(int node1, int node2);

	// Converts the grid to a matrix
	std::vector<std::string> toMatrix();

	// Gets the adjancencyLists
	std::vector<std::vector<Vertex>> & getAdjacencyLists();

	// Returns a predecessor vector, where the start node's previous position is 0
	/// Usage instructions:
	// In order to use it for the Quoridor game we need to run this method for a player
	// Then using the destination positions for that particular player, we check if they were visited
	// To check if a node has been visited you call the vector[node.getId()] and see if it doesnt equal -2
	// If ANY of the nine destinations are different from -2 then the player can get to it
	// Then we redo the steps above for the next player, until we have checked every player
	std::vector<int> generatePredecessorVector(int start);

private: 
	std::vector<std::vector<Vertex>> adjacencyLists;
};

