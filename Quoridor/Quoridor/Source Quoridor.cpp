#include "GamePlay.h"

int main()
{
	std::ofstream logFile("syslog.log", std::ios::app);
	Logger logger(logFile);

	logger.log("Started Application...", Logger::Level::Info);
	GamePlay quoridorGame;
	quoridorGame.gameOn();
	logger.log("Finished Application...", Logger::Level::Info);
	logFile.close();

	return 0;
}