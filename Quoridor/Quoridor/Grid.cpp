#include "Grid.h"

Grid::Grid()
{
	// empty
}

Grid::Grid(int numberRows)
{
	int size = numberRows * numberRows;
	adjacencyLists.resize(size);

	// Creates the lists
	for (int index = 0; index < size; ++index)
	{
		if (index < numberRows)
		{
			adjacencyLists[index].emplace_back(index, Vertex::Type::Destination2);
		}
		else if (index >= size - numberRows)
		{
			adjacencyLists[index].emplace_back(index, Vertex::Type::Destination1);
		}
		else
		{
			adjacencyLists[index].emplace_back(index);
		}
	}

	// Fills each list according to the node's position in the grid
	for (int index = 0; index < size; ++index)
	{
		// If its not in the first row, adds to the list the node above it
		if (index >= numberRows)
		{
			adjacencyLists[index].push_back(adjacencyLists[index - numberRows][0]);
		}
		// If its not in the first collumn, adds to the list the node to its left
		if (index % numberRows != 0)
		{
			adjacencyLists[index].push_back(adjacencyLists[index - 1][0]);
		}
		// If its not in the last collumn, adds to the list the node to its right
		if (index % numberRows < numberRows - 1)
		{
			adjacencyLists[index].push_back(adjacencyLists[index + 1][0]);
		}
		// If its not in the last row, adds to the list the node below it
		if (index < size - numberRows)
		{
			adjacencyLists[index].push_back(adjacencyLists[index + numberRows][0]);
		}
	}

	std::ofstream logFile("syslog.log", std::ios::app);
	Logger logger(logFile);
	logger.log("Constructed grid", Logger::Level::Info);
	logFile.close();
}


Grid::~Grid()
{
	// empty
}

Vertex & Grid::operator[](int number)
{
	return adjacencyLists[number][0];
}

void Grid::removeEdge(int node1, int node2)
{
	if ((node1 < adjacencyLists.size()) && (node2 < adjacencyLists.size()))
	{
		adjacencyLists[node1].erase(std::find(adjacencyLists[node1].begin(), adjacencyLists[node1].end(), adjacencyLists[node2][0]));
		adjacencyLists[node2].erase(std::find(adjacencyLists[node2].begin(), adjacencyLists[node2].end(), adjacencyLists[node1][0]));
	}
	std::ofstream logFile("syslog.log", std::ios::app);
	Logger logger(logFile);
}

void Grid::addEdge(int node1, int node2)
{
	if ((node1 < adjacencyLists.size()) && (node2 < adjacencyLists.size()))
	{
		adjacencyLists[node1].push_back(adjacencyLists[node2][0]);
		adjacencyLists[node2].push_back(adjacencyLists[node1][0]);
	}
	std::ofstream logFile("syslog.log", std::ios::app);
	Logger logger(logFile);
}

bool Grid::hasEdge(int node1, int node2)
{
	// We only need to check if one of the nodes is found in the other's list and not the other way around
	if (std::find(adjacencyLists[node1].begin(), adjacencyLists[node1].end(), adjacencyLists[node2][0]) != adjacencyLists[node1].end())
		return true;
	else
		return false;
}

std::vector<std::string> Grid::toMatrix()
{
	std::vector<std::string> tempMatrix;
	int positionAdjacency;
	int sizeAdjacency = std::sqrt(adjacencyLists.size());
	int size = 2 * sizeAdjacency - 1;
	char orizontal = 196, vertical = 179;
	tempMatrix.resize(size);
	for (int index = 0; index < size; index++)
	{
		tempMatrix[index].resize(size);
	}

	for (int index1 = 0; index1 < size; index1++)
	{
		for (int index2 = 0; index2 < size; index2++)
		{
			positionAdjacency = (index1 / 2) * sizeAdjacency + (index2 / 2);
			if (index1 % 2 == 0)
			{
				if (index2 % 2 == 0)
				{
					if (adjacencyLists[positionAdjacency][0].getPawn().getBody() == Pawn::Body::FirstPlayer)
					{
						tempMatrix[index1][index2] = '1';
					}
					else if (adjacencyLists[positionAdjacency][0].getPawn().getBody() == Pawn::Body::SecondPlayer)
					{
						tempMatrix[index1][index2] = '2';
					}
					else if (adjacencyLists[positionAdjacency][0].getPawn().getBody() == Pawn::Body::None)
					{
						tempMatrix[index1][index2] = 'o';
					}
					if (index2 >= 2)
					{
						if (!hasEdge(positionAdjacency - 1, positionAdjacency))
						{
							tempMatrix[index1][index2 - 1] = vertical;
						}
						else
						{
							tempMatrix[index1][index2 - 1] = ' ';
						}
					}
					if (index1 >= 2)
					{
						if (!hasEdge(positionAdjacency - sizeAdjacency, positionAdjacency))
						{
							tempMatrix[index1 - 1][index2] = orizontal;
						}
						else
						{
							tempMatrix[index1 - 1][index2] = ' ';
						}
					}
				}
			}
		}
	}

	for (int index1 = 1; index1 < size - 1; index1++)
	{
		for (int index2 = 1; index2 < size - 1; index2++)
		{
			positionAdjacency = (index1 / 2) * sizeAdjacency + (index2 / 2);
			if (index1 % 2 != 0)
			{
				if (index2 % 2 != 0)
				{
					if ((tempMatrix[index1 - 1][index2] == vertical) && (tempMatrix[index1 + 1][index2] == vertical))
					{

							tempMatrix[index1][index2] = vertical;
					}
				}
				if ((tempMatrix[index1][index2 - 1] == orizontal) && (tempMatrix[index1][index2 + 1] == orizontal))
				{

						tempMatrix[index1][index2] = orizontal;
				}
			}
		}
	}
	return tempMatrix;
}

std::vector<std::vector<Vertex>> & Grid::getAdjacencyLists()
{
	return adjacencyLists;
}

std::vector<int> Grid::generatePredecessorVector(int start)
{
	std::vector<int> visited;
	std::vector<int> notVisited;
	std::vector<int> verified;
	std::vector<int> predecessorVector;
	int count = 0;

	predecessorVector.resize(adjacencyLists.size());

	for (int index = 0; index < adjacencyLists.size(); ++index)
	{
		notVisited.push_back(index);
	}

	visited.push_back(start);
	notVisited.erase(notVisited.begin() + start);
	predecessorVector[start] = -1;

	for (int y : notVisited)
	{
		predecessorVector[y] = -2;
	}

	while (visited.size() > 0)
	{
		int x = visited[0];
		int y = 0;

		for (Vertex nod : adjacencyLists[x])
		{
			y = nod.getId();
			if (std::find(notVisited.begin(), notVisited.end(), y) != notVisited.end())
			{
				notVisited.erase(std::find(notVisited.begin(), notVisited.end(), y));
				visited.push_back(y);
				predecessorVector[y] = x;
			}
		}
		visited.erase(std::find(visited.begin(), visited.end(), x));
		verified.push_back(x);
	}

	return predecessorVector;
}

