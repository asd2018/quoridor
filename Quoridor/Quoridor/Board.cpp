#include "Board.h"

const std::pair<int, int> SECOND_PLAYER_BOUNDARY (0, 9);
const std::pair<int, int> FIRST_PLAYER_BOUNDARY (72, 80);

Board::Board(int rowCount) : grid(rowCount)
{
	std::ofstream logFile("syslog.log", std::ios::app);
	Logger logger(logFile);

	logger.log("Constructed board", Logger::Level::Info);
	logFile.close();
}

bool Board::checkWinCondition(int positionPawn, int player)
{
	std::vector<int> condition = grid.generatePredecessorVector(positionPawn);
	int begin, end;

	if (player == 1)
	{
		begin = std::get<0>(FIRST_PLAYER_BOUNDARY);
		end = std::get<1>(FIRST_PLAYER_BOUNDARY);
	}
	else
	{
		begin = std::get<0>(SECOND_PLAYER_BOUNDARY);
		end = std::get<1>(SECOND_PLAYER_BOUNDARY);
	}

	for (int index = begin; index < end; ++index)
	{
		if (condition[index] != -2)
			return true;
	}
	return false;
}

bool Board::checkWinner(Vertex nod)
{
	if (nod.getPawn().getBody() == Pawn::Body::FirstPlayer)
	{
		if (nod.getType() == Vertex::Type::Destination1)
		{
			return true;
		}
	}
	else if (nod.getPawn().getBody() == Pawn::Body::SecondPlayer)
	{
		if (nod.getType() == Vertex::Type::Destination2)
		{
			return true;
		}
	}
	return false;
}

Grid & Board::getGrid()
{
	return grid;
}

