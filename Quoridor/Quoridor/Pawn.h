#pragma once
#include <iostream>
#include <fstream>
#include "../Logging/Logging.h"

class Pawn
{
public:
	enum class Body : uint8_t 
	{
		None,
		FirstPlayer, //starts on top of the Grid (on paper)
		SecondPlayer // -||- bottom
	};

public:
	Pawn();
	Pawn(Body body);
	Pawn(Pawn && other);
	~Pawn();
	Pawn(const Pawn & other);

	Pawn & operator = (const Pawn & other);
	Pawn & operator = (Pawn && other);

	Body getBody() const;
	int getPosition() const;

	void setBody(const Body & body);
	void setPosition(int position);

	friend std::ostream & operator<<(std::ostream & os, const Pawn & pawn);

private :
	Body m_body : 2;
	uint8_t m_position;
};


