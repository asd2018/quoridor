#include "Pawn.h"

Pawn::Pawn() : Pawn(Body::None)
{
	//empty
}

Pawn::Pawn(Body body) : m_body(body)
{
	static_assert(sizeof(*this) <= 2, "This class should be 2 bytes in size");
}

Pawn::~Pawn()
{
	m_body = Body::None;
}

Pawn::Pawn(Pawn && other)
{
	*this = std::move(other);
}

Pawn::Pawn(const Pawn & other)
{
	*this = other;
}

Pawn & Pawn::operator = (const Pawn & other)
{
	m_body = other.m_body;
	return *this;
}

Pawn & Pawn::operator = (Pawn && other)
{
	std::ofstream logFile("syslog.log", std::ios::app);
	Logger logger(logFile);
	
	m_body = other.m_body;
	new(&other) Pawn;

	logger.log("Moved pawn", Logger::Level::Info);
	logFile.close();

	return *this;
}

Pawn::Body Pawn::getBody() const
{
	return m_body;
}

int Pawn::getPosition() const
{
	return this->m_position;
}

void Pawn::setBody(const Body & id)
{
	m_body = id;
}

void Pawn::setPosition(int position)
{
	this->m_position = position;
}

std::ostream & operator<<(std::ostream & os, const Pawn & pawn)
{
	return os << pawn.m_body;
}

