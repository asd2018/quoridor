#pragma once
#include "Grid.h"

class Board
{
public:
	Board(int rowCount);
	bool checkWinCondition(int positionPawn, int player);
	bool checkWinner(Vertex nod);
	Grid& getGrid();

private :
	Grid grid;
	int rowNumber;
};