#pragma once
#include<iostream>
#include<string>

#include <winsock2.h>	// contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>	// contains newer functions and structures used to retrieve IP addresses

#pragma comment(lib, "Ws2_32.lib")	//  indicates to the linker that the Ws2_32.lib

class Server
{
public:
	Server();
	~Server();

	const std::string receiveData(int& bytesReceived);
	void sendData(const char* data);
	void shutDown();

private:
	SOCKET clientSocket;
};

