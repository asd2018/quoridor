#include "Server.h"



Server::Server()
{
	const uint8_t VERSION = 2;
	// *** Initialize Winsock ***
	// initialize Winsock before making other Winsock functions calls
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(VERSION, VERSION), &wsaData);		// initiate use of WS2_32.dll, with version 2.2
	if (iResult != 0)
	{
		throw std::string("WSAStartup failed: ") + std::to_string(iResult);
	}

	// *** Create socket ***
	const char DEFAULT_PORT[] = "27015";

	struct addrinfo *result = nullptr, 
		hints;

	ZeroMemory(&hints, sizeof(hints));	// memset to 0
	hints.ai_family = AF_INET;			// IPv4
	hints.ai_socktype = SOCK_STREAM;	// stream
	hints.ai_protocol = IPPROTO_TCP;	// tcp
	hints.ai_flags = AI_PASSIVE;		// we intend to use the socket in a call to bind

	// Resolve the local address and port to be used by the server
	iResult = getaddrinfo(nullptr, DEFAULT_PORT, &hints, &result);
	if (iResult != 0)
	{
		WSACleanup();
		throw std::string("Getaddrinfo failed: ") + std::to_string(iResult);
	}

	SOCKET ListenSocket = INVALID_SOCKET;
	// Create a SOCKET for the server to listen for client connections
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET)
	{
		WSACleanup();
		freeaddrinfo(result);
		throw std::string("Error at socket(): ") + std::to_string(WSAGetLastError());
	}

	// *** Binding the socket ***
	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR)
	{
		WSACleanup();
		freeaddrinfo(result);
		closesocket(ListenSocket);
		throw std::string("Bind failed with error: ") + std::to_string(WSAGetLastError());
	}

	// 'result' not needed anymore so free it
	freeaddrinfo(result);

	// *** Listening on the socket ***
	if (listen(ListenSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		WSACleanup();
		closesocket(ListenSocket);
		throw std::string("Listen failed with error: ") + std::to_string(WSAGetLastError());
	}

	// *** Accept a connection ***
	// There are multiple techniques to accept client connections,
	// and multiple techniques to accept multiple client connections.
	// In this example however a single connection is accepted.
	clientSocket = INVALID_SOCKET;
	// Accept a client socket
	clientSocket = accept(ListenSocket, nullptr, nullptr);
	if (clientSocket == INVALID_SOCKET)
	{
		WSACleanup();
		closesocket(ListenSocket);
		throw std::string("Accept failed: ") + std::to_string(WSAGetLastError());
	}

	// No longer need server socket
	closesocket(ListenSocket);
}


Server::~Server()
{
	closesocket(clientSocket);
	WSACleanup();
}

const std::string Server::receiveData(int& bytesReceived)
{
	const int DEFAULT_BUFLEN = 512;
	char receiveBuffer[DEFAULT_BUFLEN];
	int receiveBufferLength = DEFAULT_BUFLEN;

	bytesReceived = recv(clientSocket, receiveBuffer, receiveBufferLength, 0);

	if (bytesReceived == SOCKET_ERROR)
	{
		throw std::string("Receive operation failed: ") + std::to_string(WSAGetLastError());
	}
	

	return std::string(receiveBuffer, bytesReceived);
}

void Server::sendData(const char* data)
{
	const int DEFAULT_BUFLEN = 512;
	int bytesSend;

	bytesSend = send(clientSocket, data, strlen(data), 0);

	if (bytesSend == SOCKET_ERROR)
	{
		throw std::string("Receive operation failed: ") + std::to_string(WSAGetLastError());
	}
}

void Server::shutDown()
{
	int iResult = shutdown(clientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) 
	{
		throw std::string("Shutdown failed: ") + std::to_string(WSAGetLastError());
	}
}
