#include "Board.h"

const std::pair<uint8_t, uint8_t> SECOND_PLAYER_BOUNDARY (0, 9);
const std::pair<uint8_t, uint8_t> FIRST_PLAYER_BOUNDARY (72, 80);

Board::Board()
{
	// empty
}

bool Board::checkWinCondition(uint8_t positionPawn, uint8_t player)
{
	std::vector<int16_t> condition = grid.generatePredecessorVector(positionPawn);
	uint8_t begin, end;

	if (player == 1)
	{
		begin = std::get<0>(FIRST_PLAYER_BOUNDARY);
		end = std::get<1>(FIRST_PLAYER_BOUNDARY);
	}
	else
	{
		begin = std::get<0>(SECOND_PLAYER_BOUNDARY);
		end = std::get<1>(SECOND_PLAYER_BOUNDARY);
	}

	for (uint8_t index = begin; index < end; ++index)
	{
		if (condition[index] != -2)
			return true;
	}
	return false;
}

bool Board::checkWinner(Vertex& nod)
{
	if (nod.getPawn().getBody() == Pawn::Body::FirstPlayer)
	{
		if (nod.getType() == Vertex::Type::Destination1)
		{
			return true;
		}
	}
	else if (nod.getPawn().getBody() == Pawn::Body::SecondPlayer)
	{
		if (nod.getType() == Vertex::Type::Destination2)
		{
			return true;
		}
	}
	return false;
}

Grid<Vertex, TOTAL_ROWS> & Board::getGrid()
{
	return grid;
}

