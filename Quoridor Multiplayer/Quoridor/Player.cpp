#include "Player.h"

const uint8_t TOTAL_POSITIONS = TOTAL_ROWS * TOTAL_ROWS - 1; //80, starting from 0
const uint8_t NUMBER_OF_WALLS = 10;

Player::Player(const std::string& name, const Pawn& pawn) : m_name(name), m_pawn(pawn)
{
	m_wallNumber = NUMBER_OF_WALLS;
	if (pawn.getBody() == Pawn::Body::FirstPlayer)
		m_pawnPosition = TOTAL_ROWS / 2; //4
	else
		m_pawnPosition = TOTAL_POSITIONS - TOTAL_ROWS / 2; //76
}

std::ostream& operator<<(std::ostream& os, const Player& player)
{
	os << "Player " << player.getPlayerNumber() << ": " << player.m_name;
	return os;
}

bool Player::movePawn(std::istream& in, Grid<Vertex, TOTAL_ROWS> & grid, uint8_t enemyPosition)
{
	uint8_t oldPawnPosition = m_pawnPosition;

	if (enemyPosition > 0)
	{
		m_pawnPosition = enemyPosition;
	}

	//it can move North, South, East or West just a step, if possible, meaning:
	//1: there isn't a wall, or
	//2: isn't out-of-Board's range, or
	//3: if there's the enemy pawn, he will move 2 steps, if he can.
	while(true)
	{
		Console::printMessage("Enter 'up' to move Up, 'down' - Down, 'left' - Left, 'right' - Right, 'return' - returns to initial position (the one before this move)");
		std::string move;
		in >> move;

		if (move == "up") //North
		{
			if (m_pawnPosition < TOTAL_ROWS) //Checks if moving North is still on the board
			{
				Console::printMessage("Can't move up! Out of Board!");
				continue;
			}

			if (!grid.hasEdge(m_pawnPosition, m_pawnPosition - TOTAL_ROWS)) //Checks if there's a link to the North
			{
				Console::printMessage("Can't go up because there's a wall!");
				continue;
			}

			if (grid[m_pawnPosition - TOTAL_ROWS].getPawn().getBody() != std::nullopt)
			{
				if (enemyPosition == 0)
				{
					Console::printMessage("Another pawn found on selected position! \n Starting from the other pawn select a direction");
					if (movePawn(std::cin, grid, m_pawnPosition - TOTAL_ROWS))
						break;
					else
					{
						Console::printMessage("You've returned to the initial position!");
						continue;
					}
				}
				else
				{
					Console::printMessage("Another pawn found on selected position! \n You cannot move over more than one pawn!");
					continue;
				}
			}
			std::swap(grid[m_pawnPosition - TOTAL_ROWS].getPawn(), grid[oldPawnPosition].getPawn());
			m_pawnPosition -= TOTAL_ROWS;
			break;
		}
		if (move == "right") //East
		{
			if (m_pawnPosition % TOTAL_ROWS == TOTAL_ROWS - 1) //Checks if moving East is still on the board
			{
				Console::printMessage("Can't move right! Out of Board!");
				continue;
			}

			if (!grid.hasEdge(m_pawnPosition, m_pawnPosition + 1)) //Checks if there's a link to East
			{
				Console::printMessage("Can't go right because there's a wall!");
				continue;
			}

			if (grid[m_pawnPosition + 1].getPawn().getBody() != std::nullopt)
			{
				if (enemyPosition == 0)
				{
					Console::printMessage("Another pawn found on selected position! \n Starting from the other pawn select a direction");
					if (movePawn(std::cin, grid, m_pawnPosition + 1))
						break;
					else
					{
						Console::printMessage("You've returned to the initial position!");
						continue;
					}
				}
				else
				{
					Console::printMessage("Another pawn found on selected position! \n You cannot move over more than one pawn!");
					continue;
				}
			}
			std::swap(grid[m_pawnPosition + 1].getPawn(), grid[oldPawnPosition].getPawn());
			m_pawnPosition += 1;
			break;
		}
		if (move == "down") //South
		{
			if (m_pawnPosition > TOTAL_POSITIONS - TOTAL_ROWS) //Checks if moving South is still on the board
			{
				Console::printMessage("Can't move down! Out of Board!");
				continue;
			}

			if (!grid.hasEdge(m_pawnPosition, m_pawnPosition + TOTAL_ROWS)) //Checks if there's a link to the South
			{
				Console::printMessage("Can't go down because there's a wall!");
				continue;
			}

			if (grid[m_pawnPosition + TOTAL_ROWS].getPawn().getBody() != std::nullopt)
			{
				if (enemyPosition == 0)
				{
					Console::printMessage("Another pawn found on selected position! \n Starting from the other pawn select a direction");
					if (movePawn(std::cin, grid, m_pawnPosition + TOTAL_ROWS))
						break;
					else
					{
						Console::printMessage("You've returned to the initial position!");
						continue;
					}
				}
				else
				{
					Console::printMessage("Another pawn found on selected position! \n You cannot move over more than one pawn!");
					continue;
				}
			}
			std::swap(grid[m_pawnPosition + TOTAL_ROWS].getPawn(), grid[oldPawnPosition].getPawn());
			m_pawnPosition += TOTAL_ROWS;
			break;
		}
		if (move == "left") //West
		{
			if (m_pawnPosition % TOTAL_ROWS == 0) //Checks if moving West is still on the board
			{
				Console::printMessage("Can't move left! Out of Board!");
				continue;
			}

			if (!grid.hasEdge(m_pawnPosition, m_pawnPosition - 1)) //Checks if there's a link to West

			{
				Console::printMessage("Can't go left because there's a wall!");
				continue;
			}

			if (grid[m_pawnPosition - 1].getPawn().getBody() != std::nullopt)
			{
				if (enemyPosition == 0)
				{
					Console::printMessage("Another pawn found on selected position! \n Starting from the other pawn select a direction");
					if (movePawn(std::cin, grid, m_pawnPosition - 1))
						break;
					else
					{
						Console::printMessage("You've returned to the initial position!");
						continue;
					}
				}
				else
				{
					Console::printMessage("Another pawn found on selected position! \n You cannot move over more than one pawn!");
					continue;
				}
			}
			std::swap(grid[m_pawnPosition - 1].getPawn(), grid[oldPawnPosition].getPawn());
			m_pawnPosition -= 1;
			break;
		}
		if (move == "return")
		{
			if (enemyPosition != 0)
			{
				m_pawnPosition = oldPawnPosition;
				return false;
			}
			else
			{
				Console::printMessage("Your position hasn't changed!\n Try Again!\n");
				continue;
			}
		}
		else
		{
			Console::printMessage("Incorrect input!\n Try Again!\n");
			continue;
		}
	}
	return true;
}

void Player::placeWall(const std::string& position, const std::string& position2, const std::string& direction, Board & board,const Player & enemy)
{
	//first checks if it can be placed

	std::regex valid("[a-i][1-9]");

	if ((!std::regex_match(position, valid)) || (!std::regex_match(position2, valid)))
	{
		throw (std::string)"\n Invalid input! The positions must have the following structure: CollumnRow e.g a1 \n\n";
	}

	if ((position[0] != position2[0]) && (position[1] != position2[1]))
	{
		throw (std::string)"\nInvalid input! The positions are not connected.\n "
			+ " The locations must either be on the same line or on the same collumn \n\n";
	}

	if (position == position2)
	{
		throw (std::string)"\n Invalid input! The positions are identical.\n\n ";
	}

	int node1 = position[0] - 97 + (position[1] - 49) * TOTAL_ROWS,
		node2 = position2[0] - 97 + (position2[1] - 49) * TOTAL_ROWS,
		node3, node4;

	if (!board.getGrid().hasEdge(node1, node2))
	{
		throw (std::string)"\n Invalid input! There's already a wall there! \n";
	}

	if (direction == "up")
	{
		if (position[1] == position2[1])
		{
			node3 = node1 - TOTAL_ROWS;
			node4 = node2 - TOTAL_ROWS;

			if ((node3 < 0) || (node4 < 0))
			{
				throw (std::string)"\n Invalid input! There's not enough space to place the wall! \n";
			}

			if ((!board.getGrid().hasEdge(node1, node3)) && (!board.getGrid().hasEdge(node2, node4)))
			{
				throw (std::string)"\n Invalid input! There's not enough space to place the wall! \n";
			}

			if (!board.getGrid().hasEdge(node3, node4))
			{
				throw (std::string)"\n Invalid input! There's already a wall there! \n";
			}
		}
		else
		{
			throw (std::string)"\n Invalid input! The 'up' direction indicates that the rows must be equal.\n\n ";
		}
	}
	else if (direction == "down")
	{
		if (position[1] == position2[1])
		{
			node3 = node1 + TOTAL_ROWS;
			node4 = node2 + TOTAL_ROWS;

			if ((node3 > TOTAL_POSITIONS) || (node4 > TOTAL_POSITIONS))
			{
				throw (std::string)"\n Invalid input! There's not enough space to place the wall! \n";
			}

			if ((!board.getGrid().hasEdge(node1, node3)) && (!board.getGrid().hasEdge(node2, node4)))
			{
				throw (std::string)"\n Invalid input! There's not enough space to place the wall! \n";
			}

			if (!board.getGrid().hasEdge(node3, node4))
			{
				throw (std::string)"\n Invalid input! There's already a wall there! \n";
			}
		}
		else
		{
			throw (std::string)"\n Invalid input! The 'down' direction indicates that the rows must be equal.\n\n ";
		}
	}
	else if (direction == "left")
	{
		if (position[0] == position2[0])
		{
			if ((node1 % TOTAL_ROWS == 0) || (node2 % TOTAL_ROWS == 0))
			{
				throw (std::string)"\n Invalid input! There's not enough space to place the wall! \n";
			}

			node3 = node1 - 1;
			node4 = node2 - 1;

			if ((!board.getGrid().hasEdge(node1, node3)) && (!board.getGrid().hasEdge(node2, node4)))
			{
				throw (std::string)"\n Invalid input! There's not enough space to place the wall! \n";
			}

			if (!board.getGrid().hasEdge(node3, node4))
			{
				throw (std::string)"\n Invalid input! There's already a wall there! \n";
			}
		}
		else
		{
			throw (std::string)"\n Invalid input! The 'left' direction indicates that the columns must be equal.\n\n ";
		}
	}
	else if (direction == "right")
	{
		if (position[0] == position2[0])
		{
			if ((node1 % TOTAL_ROWS == TOTAL_ROWS - 1) || (node2 % TOTAL_ROWS == TOTAL_ROWS - 1))
			{
				throw (std::string)"\n Invalid input! There's not enough space to place the wall! \n";
			}

			node3 = node1 + 1;
			node4 = node2 + 1;

			if ((!board.getGrid().hasEdge(node1, node3)) && (!board.getGrid().hasEdge(node2, node4)))
			{
				throw (std::string)"\n Invalid input! There's not enough space to place the wall! \n";
			}

			if (!board.getGrid().hasEdge(node3, node4))
			{
				throw (std::string)"\n Invalid input! There's already a wall there! \n";
			}
		}
		else
		{
			throw (std::string)"\n Invalid input! The 'right' direction indicates that the columns must be equal.\n\n ";
		}
	}
	else
		throw (std::string)"\n Invalid input! The direction must be 'up', 'down', 'left' or 'right'! \n\n";

	board.getGrid().removeEdge(node1, node2);
	board.getGrid().removeEdge(node3, node4);

	if ((!board.checkWinCondition(m_pawnPosition, getPlayerNumber())) ||
		(!board.checkWinCondition(enemy.getPawnPosition(), enemy.getPlayerNumber())))
	{
		board.getGrid().addEdge(node1, node2);
		board.getGrid().addEdge(node3, node4);
		throw (std::string) "\n Cannot place wall as it blocks the way of one of the players! \n\n";
	}
	
	m_wallNumber--;
	return;

}

const uint8_t Player::getWallNumber() const
{
	return m_wallNumber;
} 

const uint8_t Player::getPawnPosition() const
{
	return m_pawnPosition;
} 

const uint8_t Player::getPlayerNumber() const
{
	if (m_pawn.getBody() == Pawn::Body::FirstPlayer)
		return 1;
	else
		return 2;
} 

const std::string & Player::getName() const
{
	return m_name;
}

void Player::setPawnPosition(uint8_t newPawnPosition)
{
	m_pawnPosition = newPawnPosition;
}
