#pragma once
#include <regex>
#include <string>
#include "Board.h"
#include "../ConsolePrint/Console.h"

class Player
{
public:
	Player(const std::string& name, const Pawn& pawn);
	bool movePawn(std::istream& in, Grid<Vertex, TOTAL_ROWS> &grid, uint8_t enemyPosition = 0);
	void placeWall(const std::string& position, const std::string& position2, const std::string& direction, Board & board, const Player & enemy);
	const uint8_t getWallNumber() const;
	const uint8_t getPawnPosition() const;
	const uint8_t getPlayerNumber() const;
	const std::string& getName() const;
	void setPawnPosition(uint8_t newPawnPosition);

	friend std::ostream& operator << (std::ostream& os, const Player& player);

private:
	std::string m_name;
	uint8_t m_pawnPosition;
	Pawn m_pawn;
	uint8_t m_wallNumber;
};
