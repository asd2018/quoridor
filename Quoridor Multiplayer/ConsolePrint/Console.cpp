#include "Console.h"



Console::Console()
{
	// empty
}


Console::~Console()
{
	// empty
}

void Console::printMatrix(const std::vector<std::string> & matrix)
{
	int size = matrix.size() + 2;
	char orizontal = 205, vertical = 186, stangaSus = 201, dreaptaSus = 187, stangaJos = 200, dreaptaJos = 188;

	for (int index1 = 0; index1 < size; ++index1)
	{
		for (int index2 = 0; index2 < size; ++index2)
		{
			switch (index1)
			{
			case 0:
			{
				if (index2 > 0)
				{
					if (index2 % 2 == 0)
					{
						std::cout << (char)(96 + index2 / 2);
					}
					else
					{
						std::cout << " ";
					}
				}
				else
				{
					std::cout << " ";
				}
				break;
			}
			case 1:
			{
				switch (index2)
				{
				case 0: std::cout << " "; break;
				case 1: std::cout << stangaSus; break;
				default: std::cout << orizontal; break;
				}
				if (index2 == size - 1)
				{
					std::cout << dreaptaSus;
				}
				break;
			}
			default:
			{
				switch (index2)
				{
				case 0:
				{
					if (index1 > 0)
					{
						if (index1 % 2 == 0)
						{
							std::cout << index1 / 2;
						}
						else
						{
							std::cout << " ";
						}
					}
					else
					{
					}
					break;
				}
				case 1:
				{
					std::cout << vertical;
					break;
				}
				default:
				{
					std::cout << matrix[index1 - 2][index2 - 2];
					if (index2 == size - 1)
					{
						std::cout << vertical;
					}
					break;
				}
				}
				break;
			}
			}
		}
		std::cout << std::endl;
	}
	std::cout << " " << stangaJos;
	for (int index = 0; index < size - 2; ++index)
		std::cout << orizontal;
	std::cout << dreaptaJos;
}

void Console::printTitleScreen()
{
	system("cls");
	std::cout
		<< "	   _ _                                     _        _\n"
		<< "	 _|_|_|_     _     _     _ _     _   _ _  |_|   _ _|_|    _ _    _   _ _\n"
		<< "	|_|  _|_|   |_|   |_|   |_|_|   |_|_|_|_|  _   |_|_|_|   |_|_|  |_|_|_|_|\n"
		<< "	|_| |_|_|   |_|   |_|  |_| |_|  |_|_|     |_| |_|   |_| |_| |_| |_|_|\n"
		<< "	|_|_ _|_|_  |_|_ _|_|  |_| |_|  |_|       |_| |_|   |_| |_| |_| |_|\n"
		<< "	  |_|_| |_|  |_|_|_|    |_|_|   |_|       |_|  |_|_|_|   |_|_|  |_|\n"
		<< std::endl << std::endl << std::endl << std::endl
		<< "			     Press any button to start\n";

	_getch();
}

void Console::printMainMenu()
{
	system("cls");

	std::cout << "~~~~~~~~~~~~~~~~ Main Menu ~~~~~~~~~~~~~~~~\n"
		<< "\n"
		<< "	1. Start game \n"
		<< "	2. Help \n"
		<< "	0. Quit game \n";
}

void Console::printHelp()
{
	system("cls");

	std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Help ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
		<< "\n"
		<< " This game is a 2 player turn-based game.\n"
		<< " Your goal is to move your pawn to any space on the opposite side of the gameboard from the starting position.\n\n"
		<< " Each player can do only ONE action per turn:\n"
		<< "	1. You can move your pawn either Up, Down, Left or Right\n"
		<< "		a. You cannot move your pawn over a wall or outside the playing field\n"
		<< "		b. If, while moving, your pawn encounters another pawn, you can step over it in any direction, while respecting the other rules\n"
		<< "	2. You can place a wall on the board:\n"
		<< "		a. A wall must occupy exactly two spaces\n"
		<< "		b. The walls cannot overlap\n"
		<< "		c. The walls must not completely block the destination of any player\n"
		<< "		d. Each player has exactly 10 walls.\n";

	std::cout << " Return to main menu by entering 'M' \n";
}

void Console::printMessage(const std::string & message)
{
	std::cout << "\n" << message << "\n";
}

void Console::printWin(const std::string & playerName)
{
	std::cout << "\n" << playerName << " Wins! \n"
		<< " Press any key to exit\n";
}

void Console::printUI(int walls,const std::string & playerName, int playerNumber)
{
	std::cout << std::endl << "Player " << playerNumber << ": " << playerName << " |" << " Walls left: " << walls << std::endl;

	std::cout << "Commands: \n"
		<< "1 - Move pawn\n";
	if (walls != 0)
		std::cout << "2 - Add wall\n";
}
