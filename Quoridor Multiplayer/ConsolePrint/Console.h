#pragma once
#include<vector>
#include<string>
#include<iostream>
#include<conio.h>
#include<iomanip>

#ifdef CONSOLEPRINT_EXPORTS
	#define CONSOLE_API __declspec(dllexport)
#else
	#define CONSOLE_API __declspec(dllimport)
#endif

class CONSOLE_API Console
{
public:
	Console();
	~Console();

	static void printMatrix(const std::vector<std::string>& matrix);

	static void printTitleScreen();

	static void printMainMenu();

	static void printHelp();

	static void printUI(int walls, const std::string& playerName, int playerNumber);

	static void printMessage(const std::string& message);

	static void printWin(const std::string& playerName);
};

