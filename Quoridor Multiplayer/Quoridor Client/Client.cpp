#include "Client.h"



Client::Client(int argc, char* argv[])
{
	const int VERSION = 2;

	// Validate the parameters
	if (argc != 2) 
	{
		throw std::string("No IP address introduced in command-line\n");
	}

	// *** initialize Winsock ***
	// initialize Winsock before making other Winsock functions calls (only once per application/dll)
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(VERSION, VERSION), &wsaData);		// initiate use of WS2_32.dll, with version 2.2
	if (iResult != 0)
	{
		throw std::string("WSAStartup failed: ") + std::to_string(iResult);
	}

	// *** Create a socket ***
	addrinfo *result = nullptr, 
		*pointer = nullptr, 
		hints;

	ZeroMemory(&hints, sizeof(hints));	// memset to 0
	hints.ai_family = AF_UNSPEC;		// unspecified: either an IPv6 or IPv4
	hints.ai_socktype = SOCK_STREAM;	// stream socket
	hints.ai_protocol = IPPROTO_TCP;	// TCP protocol

	const char DEFAULT_PORT[] = "27015";

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0)
	{
		WSACleanup();	// note: use WSACleanup when done working with sockets
		throw std::string("Getaddrinfo failed: ") + std::to_string(iResult);
	}

	connectSocket = INVALID_SOCKET;
	// Attempt to connect to the first address returned by the call to getaddrinfo
	pointer = result;

	while (pointer != nullptr)
	{
		// Create a SOCKET for connecting to server
		connectSocket = socket(pointer->ai_family, pointer->ai_socktype, pointer->ai_protocol);

		if (connectSocket == INVALID_SOCKET)
		{
			freeaddrinfo(result);
			WSACleanup();
			throw std::string("Error at socket(): ") + std::to_string(WSAGetLastError());
		}

		// *** Connect to server ***
		iResult = connect(connectSocket, pointer->ai_addr, (int)pointer->ai_addrlen);
		if (iResult == SOCKET_ERROR)
		{
			closesocket(connectSocket);
			connectSocket = INVALID_SOCKET;
		}

		if (connectSocket != INVALID_SOCKET)
		{
			break;
		}
		pointer = pointer->ai_next;
	}

	freeaddrinfo(result);

	if (connectSocket == INVALID_SOCKET)
	{
		WSACleanup();
		throw std::string("Unable to connect to server!\n");
	}
}


Client::~Client()
{
	closesocket(connectSocket);
	WSACleanup();
}

const std::string Client::receiveData(int& bytesReceived)
{
	const int DEFAULT_BUFLEN = 512;
	char receiveBuffer[DEFAULT_BUFLEN];
	int receiveBufferLength = DEFAULT_BUFLEN;

	bytesReceived = recv(connectSocket, receiveBuffer, receiveBufferLength, 0);

	if (bytesReceived == SOCKET_ERROR)
	{
		throw std::string("Receive operation failed: ") + std::to_string(WSAGetLastError());
	}


	return std::string(receiveBuffer, bytesReceived);
}

int Client::sendData(const char* data)
{
	const int DEFAULT_BUFLEN = 512;
	int bytesSend;

	bytesSend = send(connectSocket, data, strlen(data), 0);

	if (bytesSend == SOCKET_ERROR)
	{
		throw std::string("Receive operation failed: ") + std::to_string(WSAGetLastError());
	}
	return bytesSend;
}

void Client::shutDown()
{
	int iResult = shutdown(connectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) 
	{
		throw std::string("Shutdown failed: ") + std::to_string(WSAGetLastError());
	}
}
