#pragma once
#include "Vertex.h"
#include <vector>
#include <algorithm>
#include <array>

// class declaration

// This is a graph where each node can have a maximum of 4 connections
// It is a graph that looks like a squared matrix
template <class VertexType, int rowSize>
class Grid
{
public:
	Grid();
	// Creates a numberRows * numberRows size grid
	~Grid();

	// With this operator we can access directly an element of the grid
	Vertex & operator[] (uint8_t number);

	// Removes the edge between the first node and the second
	void removeEdge(uint8_t node1, uint8_t node2);

	// Adds an edge between the first node and the second
	void addEdge(uint8_t node1, uint8_t node2);

	// Checks if the two nodes have an edge connecting them
	bool hasEdge(uint8_t node1, uint8_t node2);

	// Converts the grid to a matrix
	std::vector<std::string> toMatrix();

	// Gets the adjancencyLists
	std::array<std::vector<Vertex>, rowSize * rowSize> & getAdjacencyLists();

	// Returns a predecessor vector, where the start node's previous position is 0
	/// Usage instructions:
	// In order to use it for the Quoridor game we need to run this method for a player
	// Then using the destination positions for that particular player, we check if they were visited
	// To check if a node has been visited you call the vector[node.getId()] and see if it doesnt equal -2
	// If ANY of the nine destinations are different from -2 then the player can get to it
	// Then we redo the steps above for the next player, until we have checked every player
	std::vector<int16_t> generatePredecessorVector(uint8_t start);

private: 
	std::array<std::vector<Vertex>, rowSize * rowSize> adjacencyLists;
};

// class implementation

template <class VertexType, int rowSize>
Grid<VertexType, rowSize>::Grid()
{
	uint8_t fullSize = rowSize * rowSize;

	// Creates the lists
	for (uint8_t index = 0; index < fullSize; ++index)
	{
		if (index < rowSize)
		{
			adjacencyLists[index].emplace_back(index, VertexType::Type::Destination2);
		}
		else if (index >= fullSize - rowSize)
		{
			adjacencyLists[index].emplace_back(index, VertexType::Type::Destination1);
		}
		else
		{
			adjacencyLists[index].emplace_back(index);
		}
	}

	// Fills each list according to the node's position in the grid
	for (uint8_t index = 0; index < fullSize; ++index)
	{
		// If its not in the first row, adds to the list the node above it
		if (index >= rowSize)
		{
			adjacencyLists[index].push_back(adjacencyLists[index - rowSize][0]);
		}
		// If its not in the first collumn, adds to the list the node to its left
		if (index % rowSize != 0)
		{
			adjacencyLists[index].push_back(adjacencyLists[index - 1][0]);
		}
		// If its not in the last collumn, adds to the list the node to its right
		if (index % rowSize < rowSize - 1)
		{
			adjacencyLists[index].push_back(adjacencyLists[index + 1][0]);
		}
		// If its not in the last row, adds to the list the node below it
		if (index < fullSize - rowSize)
		{
			adjacencyLists[index].push_back(adjacencyLists[index + rowSize][0]);
		}
	}
}

template <class VertexType, int rowSize>
Grid<VertexType, rowSize>::~Grid()
{
	// empty
}

template <class VertexType, int rowSize>
Vertex & Grid<VertexType, rowSize>::operator[](uint8_t number)
{
	return adjacencyLists[number][0];
}

template <class VertexType, int rowSize>
void Grid<VertexType, rowSize>::removeEdge(uint8_t node1, uint8_t node2)
{
	if ((node1 < adjacencyLists.size()) && (node2 < adjacencyLists.size()))
	{
		adjacencyLists[node1].erase(std::find(adjacencyLists[node1].begin(), adjacencyLists[node1].end(), adjacencyLists[node2][0]));
		adjacencyLists[node2].erase(std::find(adjacencyLists[node2].begin(), adjacencyLists[node2].end(), adjacencyLists[node1][0]));
	};
}

template <class VertexType, int rowSize>
void Grid<VertexType, rowSize>::addEdge(uint8_t node1, uint8_t node2)
{
	if ((node1 < adjacencyLists.size()) && (node2 < adjacencyLists.size()))
	{
		adjacencyLists[node1].push_back(adjacencyLists[node2][0]);
		adjacencyLists[node2].push_back(adjacencyLists[node1][0]);
	}
}

template <class VertexType, int rowSize>
bool Grid<VertexType, rowSize>::hasEdge(uint8_t node1, uint8_t node2)
{
	// We only need to check if one of the nodes is found in the other's list and not the other way around
	if (std::find(adjacencyLists[node1].begin(), adjacencyLists[node1].end(), adjacencyLists[node2][0]) != adjacencyLists[node1].end())
		return true;
	else
		return false;
}

template <class VertexType, int rowSize>
std::vector<std::string> Grid<VertexType, rowSize>::toMatrix()
{
	std::vector<std::string> tempMatrix;
	int16_t positionAdjacency;
	int16_t sizeAdjacency = std::sqrt(adjacencyLists.size());
	int16_t size = 2 * sizeAdjacency - 1;
	char orizontal = 196, vertical = 179;
	tempMatrix.resize(size);
	for (int16_t index = 0; index < size; index++)
	{
		tempMatrix[index].resize(size);
	}

	for (int16_t index1 = 0; index1 < size; index1++)
	{
		for (int16_t index2 = 0; index2 < size; index2++)
		{
			positionAdjacency = (index1 / 2) * sizeAdjacency + (index2 / 2);
			if (index1 % 2 == 0)
			{
				if (index2 % 2 == 0)
				{
					if (adjacencyLists[positionAdjacency][0].getPawn().getBody() == Pawn::Body::FirstPlayer)
					{
						tempMatrix[index1][index2] = '1';
					}
					else if (adjacencyLists[positionAdjacency][0].getPawn().getBody() == Pawn::Body::SecondPlayer)
					{
						tempMatrix[index1][index2] = '2';
					}
					else if (adjacencyLists[positionAdjacency][0].getPawn().getBody() == std::nullopt)
					{
						tempMatrix[index1][index2] = 'o';
					}
					if (index2 >= 2)
					{
						if (!hasEdge(positionAdjacency - 1, positionAdjacency))
						{
							tempMatrix[index1][index2 - 1] = vertical;
						}
						else
						{
							tempMatrix[index1][index2 - 1] = ' ';
						}
					}
					if (index1 >= 2)
					{
						if (!hasEdge(positionAdjacency - sizeAdjacency, positionAdjacency))
						{
							tempMatrix[index1 - 1][index2] = orizontal;
						}
						else
						{
							tempMatrix[index1 - 1][index2] = ' ';
						}
					}
				}
			}
		}
	}

	for (int16_t index1 = 1; index1 < size - 1; index1++)
	{
		for (int16_t index2 = 1; index2 < size - 1; index2++)
		{
			positionAdjacency = (index1 / 2) * sizeAdjacency + (index2 / 2);
			if (index1 % 2 != 0)
			{
				if (index2 % 2 != 0)
				{
					if ((tempMatrix[index1 - 1][index2] == vertical) && (tempMatrix[index1 + 1][index2] == vertical))
					{

						tempMatrix[index1][index2] = vertical;
					}
				}
				if ((tempMatrix[index1][index2 - 1] == orizontal) && (tempMatrix[index1][index2 + 1] == orizontal))
				{

					tempMatrix[index1][index2] = orizontal;
				}
			}
		}
	}
	return tempMatrix;
}

template <class VertexType, int rowSize>
std::array<std::vector<Vertex>, rowSize * rowSize> & Grid<VertexType, rowSize>::getAdjacencyLists()
{
	return adjacencyLists;
}

template <class VertexType, int rowSize>
std::vector<int16_t> Grid<VertexType, rowSize>::generatePredecessorVector(uint8_t start)
{
	const int8_t START_POSITION = -1, NOT_VISITED = -2;
	std::vector<uint8_t> visited;
	std::vector<uint8_t> notVisited;
	std::vector<uint8_t> verified;
	std::vector<int16_t> predecessorVector;
	uint8_t count = 0;

	predecessorVector.resize(adjacencyLists.size());

	for (uint8_t index = 0; index < adjacencyLists.size(); ++index)
	{
		notVisited.push_back(index);
	}

	visited.push_back(start);
	notVisited.erase(notVisited.begin() + start);
	predecessorVector[start] = START_POSITION;

	for (uint8_t y : notVisited)
	{
		predecessorVector[y] = NOT_VISITED;
	}

	while (visited.size() > 0)
	{
		uint8_t x = visited[0];
		uint8_t y = 0;

		for (VertexType nod : adjacencyLists[x])
		{
			y = nod.getId();
			if (std::find(notVisited.begin(), notVisited.end(), y) != notVisited.end())
			{
				notVisited.erase(std::find(notVisited.begin(), notVisited.end(), y));
				visited.push_back(y);
				predecessorVector[y] = x;
			}
		}
		visited.erase(std::find(visited.begin(), visited.end(), x));
		verified.push_back(x);
	}

	return predecessorVector;
}


