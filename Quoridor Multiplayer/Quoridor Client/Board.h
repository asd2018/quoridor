#pragma once
#include "Grid.h"

const uint8_t TOTAL_ROWS = 9;

class Board
{
public:
	Board();
	bool checkWinCondition(uint8_t positionPawn, uint8_t player);
	bool checkWinner(Vertex& nod);
	Grid<Vertex, TOTAL_ROWS>& getGrid();

private :
	Grid<Vertex, TOTAL_ROWS> grid;
};