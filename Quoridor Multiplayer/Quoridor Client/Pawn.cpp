#include "Pawn.h"

Pawn::Pawn()
{
	//empty
}

Pawn::Pawn(std::optional<Body> body) : m_body(body)
{
	static_assert(sizeof(*this) <= 3, "This class should be 3 bytes in size");
}

Pawn::~Pawn()
{
	//empty
}

Pawn::Pawn(Pawn && other)
{
	*this = std::move(other);
}

Pawn::Pawn(const Pawn & other)
{
	*this = other;
}

Pawn & Pawn::operator = (const Pawn & other)
{
	m_body = other.m_body;
	return *this;
}

Pawn & Pawn::operator = (Pawn && other)
{
	m_body = other.m_body;
	new(&other) Pawn;
	return *this;
}

const std::optional<Pawn::Body> Pawn::getBody() const
{
	return m_body;
}


void Pawn::setBody(Body id)
{
	m_body = id;
}


