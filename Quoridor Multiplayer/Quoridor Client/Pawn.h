#pragma once
#include <iostream>
#include <fstream>
#include <optional>

class Pawn
{
public:
	enum class Body : uint8_t 
	{
		FirstPlayer = 1, //starts on top of the Grid (on paper)
		SecondPlayer // -||- bottom
	};

public:
	Pawn();
	Pawn(std::optional<Body> body);
	Pawn(Pawn && other);
	~Pawn();
	Pawn(const Pawn & other);

	Pawn & operator = (const Pawn & other);
	Pawn & operator = (Pawn && other);

	const std::optional<Body> getBody() const;

	void setBody(Body body);

private :
	std::optional<Body> m_body;
};


