#include "GamePlay.h"
#include <memory>
#include "Client.h"

const uint8_t FIRST_PAWN_POSITION = TOTAL_ROWS / 2; //4
const uint8_t SECOND_PAWN_POSITION = TOTAL_ROWS * TOTAL_ROWS - (TOTAL_ROWS + 1) / 2; //76

bool mainMenu()
{
	char temp;
	while (true)
	{
		Console::printMainMenu();
		while (true)
		{
			try
			{
				std::cin >> temp;
				if ((temp != '0') && (temp != '1') && (temp != '2'))
					throw (std::string) "This choice does not exist! Try again!";
			}
			catch (std::string error)
			{
				Console::printMessage(error);
				continue;
			}
			break;
		}
		switch (temp)
		{
		case '0':
		{
			Console::printMessage("Exiting...");
			return false;
		}
		case '1':
		{
			return true;
		}
		case '2':
		{
			Console::printHelp();
			while (true)
			{
				char decisionChar;
				std::cin >> decisionChar;
				if (decisionChar == 'M')
				{
					break;
				}
			}
			break;
		}
		}
	}
}

void GamePlay::gameOn(int argc, char* argv[])
{
	const uint8_t POSITION_SIZE = 2,
		BEGINNING_POSITION = 0,
		RECEIVE_DECISION_SIZE = 1;

	uint8_t decision, valueExit = 0;
	char temp;

	Console::printTitleScreen();

	if (!mainMenu())
		return;

	Console::printMessage("Waiting for the other player to connect...");

	std::unique_ptr<Client> pClient;

	try
	{
		pClient = std::make_unique<Client>(argc, argv);
	}
	catch (std::string error)
	{
		std::cout << error << std::endl;
		return;
	}

	Board board;
	Pawn firstPawn, secondPawn;
	std::string position, position2, direction;
	int playerNumber = 1, playerTwo = 2, bytesReceived;

	firstPawn.setBody(Pawn::Body::FirstPlayer);
	board.getGrid()[FIRST_PAWN_POSITION].setPawn(Pawn::Body::FirstPlayer);

	secondPawn.setBody(Pawn::Body::SecondPlayer);
	board.getGrid()[SECOND_PAWN_POSITION].setPawn(Pawn::Body::SecondPlayer);

	std::string playerName;
	system("cls");

	Console::printMessage("Waiting for the other player to input their name...");

	try
	{
		playerName = pClient->receiveData(bytesReceived);
	}
	catch (std::string error)
	{
		std::cout << error << std::endl;
		return;
	}

	Console::printMessage("Player 1 name: " + playerName);
	Player firstPlayer(playerName, firstPawn);

	Console::printMessage("Player 2 name: ");
	std::cin >> playerName;
	Player secondPlayer(playerName, secondPawn);

	try
	{
		playerName = pClient->sendData(playerName.c_str());
	}
	catch (std::string error)
	{
		std::cout << error << std::endl;
		return;
	}
	Console::printMessage("Press any key to begin.");
	_getch();

	// Game loop
	while (true)
	{
		std::string data;
		Console::printMessage("Waiting for the other player to make their move...");
		data = pClient->receiveData(bytesReceived);

		// Receive data
		switch (data[BEGINNING_POSITION])
		{
		case '0': Console::printWin(firstPlayer.getName()); pClient->shutDown();  _getch(); Console::printMessage("Exiting..."); return;
		case '1':
		{
			data.erase(BEGINNING_POSITION, RECEIVE_DECISION_SIZE);
			uint8_t newPosition = std::stoi(data);
			std::swap(board.getGrid()[newPosition].getPawn(), board.getGrid()[firstPlayer.getPawnPosition()].getPawn());
			firstPlayer.setPawnPosition(newPosition);
			break;
		}
		case '2':
		{
			data.erase(BEGINNING_POSITION, RECEIVE_DECISION_SIZE);

			position = data.substr(BEGINNING_POSITION, POSITION_SIZE);
			data.erase(BEGINNING_POSITION, POSITION_SIZE);

			position2 = data.substr(BEGINNING_POSITION, POSITION_SIZE);
			data.erase(BEGINNING_POSITION, POSITION_SIZE);

			direction = data;
			firstPlayer.placeWall(position, position2, direction, board, secondPlayer);
			break;
		}
		}
		std::swap(firstPlayer, secondPlayer);
		std::swap(playerNumber, playerTwo);
		system("cls");

		Console::printMatrix(board.getGrid().toMatrix());
		Console::printUI(firstPlayer.getWallNumber(), firstPlayer.getName(), firstPlayer.getPlayerNumber());

		// Checks if the input is good
		// This will loop until the player inserts the right input
		while (true)
		{
			try
			{
				std::cin >> temp;
				if ((temp != '1') && (temp != '2'))
					throw (std::string) "This choice does not exist! Try again!";
				if ((temp == '2') && (firstPlayer.getWallNumber() == 0))
					throw (std::string) "This choice does not exist! Try again!";
			}
			catch (std::string error)
			{
				Console::printMessage(error);
				continue;
			}
			break;
		}

		decision = temp - 48;

		switch (decision - 1)
		{
		case 0:
		{
			firstPlayer.movePawn(std::cin, board.getGrid());
			break;
		}
		case 1:
		{
			while (true)
			{
				Console::printMessage("Specify the starting positions(e.g. a1 b1) and direction for the wall\n The directions are: up, down, left, right");
				std::cin >> position >> position2 >> direction;
				try
				{
					firstPlayer.placeWall(position, position2, direction, board, secondPlayer);
				}
				catch (std::string error)
				{
					Console::printMessage(error + "Try again!");
					continue;
				}
				break;
			}
			break;
		}
		}

		// Victory condition check
		if (board.checkWinner(board.getGrid()[firstPlayer.getPawnPosition()]))
		{
			system("cls");
			Console::printMatrix(board.getGrid().toMatrix());
			Console::printWin(firstPlayer.getName());
			pClient->sendData("0");
			pClient->shutDown();
			_getch();
			Console::printMessage("Exiting...");
			return;
		}

		// Send data
		switch (decision - 1)
		{
		case 0:
		{
			std::string temp = "1" + std::to_string(firstPlayer.getPawnPosition());
			try
			{
				pClient->sendData(temp.c_str());
			}
			catch (std::string error)
			{
				std::cout << error << std::endl;
				pClient->shutDown();
				return;
			}
			break;
		}
		case 1:
		{
			std::string temp = "2" + position + position2 + direction;
			try
			{
				pClient->sendData(temp.c_str());
			}
			catch (std::string error)
			{
				std::cout << error << std::endl;
				pClient->shutDown();
				return;
			}
			break;
		}
		}

		// Changes the active player
		std::swap(firstPlayer, secondPlayer);
		std::swap(playerNumber, playerTwo);

		system("cls");
		Console::printMatrix(board.getGrid().toMatrix());
	}
}

